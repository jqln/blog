var gulp = require('gulp');
var watch = require('gulp-watch');
var sass = require('gulp-sass');
var responsive = require('gulp-responsive');
var imagemin = require('gulp-imagemin');

var OUT_DIR = 'out/intermediates';

/*
 * Compilation tasks.
 *
 * These involve non-trivial transformations of source files. Compiling,
 * minifying, resizing, generating. These are the most interesting rules.
 */

gulp.task('compile-sass', function() {
  var options = {
    outputStyle: 'compressed',
  };
  return gulp.src('src-web/css/**/*.scss')
    .pipe(sass(options).on('error', sass.logError))
    .pipe(gulp.dest(OUT_DIR + '/css'));
});

// Helper function for the resize-images task. Returns functions that prepends
// a base directory name onto a path.
var renamer = function(basename) {
  return function(path) {
    path.dirname = basename + "/" + path.dirname;
    return path;
  }
}

gulp.task('resize-images', function() {
  return gulp.src('src-web/img-resize/**/*')
    .pipe(responsive({
      '**/*': [
        {
          height: 256,
          rename: renamer("thumb"),
          withoutEnlargement: true,
          errorOnEnlargement: false
        },
        {
          height: 1024,
          rename: renamer("full"),
          withoutEnlargement: true,
          errorOnEnlargement: false
        }
      ]
    }))
    .pipe(imagemin({progressive: true}))
    .pipe(gulp.dest(OUT_DIR + '/img'))
});

/*
 * Copy tasks.
 *
 * For development files are kept in a number of different directories, but they
 * need to be squashed together in prod. In all cases we're not doing anything
 * special to the files, just copying.
 */

gulp.task('copy-css', function() {
  return gulp.src('src-web/css/**/*.css')
    .pipe(gulp.dest(OUT_DIR + '/css'));
});

gulp.task('copy-img', function() {
  return gulp.src('src-web/img/**/*')
    .pipe(imagemin({progressive: true}))
    .pipe(gulp.dest(OUT_DIR + '/img'));
});

gulp.task('copy-js', function() {
  return gulp.src('src-web/js/**/*.js')
    .pipe(gulp.dest(OUT_DIR + '/js'));
});

gulp.task('copy-root', function() {
  return gulp.src('src-web/root/**/*')
    .pipe(gulp.dest(OUT_DIR));
});

/**
 * Build target tasks
 *
 * These are the tasks that should actually be run.
 */

gulp.task('watch', function() {
  gulp.watch('src-web/css/**/*.scss', gulp.series('compile-sass'));
  gulp.watch('src-web/css-partials/**/*.scss', gulp.series('compile-sass'));
  gulp.watch('src-web/img-resize/**/*', gulp.series('resize-images'));
  gulp.watch('src-web/css/**/*.css', gulp.series('copy-css'));
  gulp.watch('src-web/img/**/*', gulp.series('copy-img'));
  gulp.watch('src-web/js/**/*.js', gulp.series('copy-js'));
  gulp.watch('src-web/root/**/*', gulp.series('copy-root'));
});

gulp.task('build', gulp.series(
  'compile-sass',
  'resize-images',
  'copy-css',
  'copy-img',
  'copy-js',
  'copy-root'
));

gulp.task('default', gulp.series('build', 'watch'));

