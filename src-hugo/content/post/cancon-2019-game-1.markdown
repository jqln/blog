+++
Tags = ["infinity", "Cancon 2019"]
date = "2019-02-01"
draft = false
title = "Cancon 2019: Game One (Supremacy)"
+++

Unfortunately, I forgot my opponent's name for the first game! I think he was from Melbourne? His ITS name was 'RussM' according to the OTM, so I'll just refer to him as Russ here. He was running Morats.

We played on a neat ruins table that I'd played on already during the narrative event the night before:

{{< img src="cancon-2019/round-1-table.jpg" title="Table for game 1. Photo by Mark Fabian, table by Jacob Everingham" >}}

<!--more-->

Russ chose to use a limited insertion list against me. The list was something like this:

{{< img-noscale src="cancon-2019/round-1-list.png" title="An estimate of my opponent's list" >}}

(Probably not quite right. In particular, I don't think the SWC allocation is correct. I don't remember the Rasyat having a Spitfire.)

Russ won the Lt roll-off and chose first turn. I chose to deploy second, on the side with the big two-storey ruin in the middle of the deployment zone (the closest side in the photo).

Russ deployed his core link very aggressively; mostly out in the open on my left, on the edge of his deployment zone. The MI link members were forward deployed as far forward as he could get them. His XT was synced to the Zerat, which deployed on the far right side of the table with his Daturazi.

I deployed my Myrmidon link in the centre two-story building, with Phoenix looking out the window watching over the right of my deployment zone and a Myrmidon looking out the left. They were still hidden enough that Russ would basically have to be inside my deployment zone to get a line on them.

My Thorakitai link went down in the building on the right side of my deployment zone. Thrasymedes was looking out the door at the big ruined building on my right of the table, but otherwise, everyone was hiding prone.

My Netrods both failed their rolls but dispersed into perfectly safe spots behind buildings in my deployment zone. My Warcor went on the second floor of the same building the Myrmidons were in, such that he could move the Xenotech into the closest quadrant without breaking coherency.

My sensor and flash pulse bots were deployed peeking out from corners in my deployment zone, covering the fire lanes that Russ would have to go down in order to engage either of my link teams.

Russ' reserve was his Evo remote, which he placed on the back left of the table, in amongst some of his core link members.

My reserve deployment was my Agema, and I decided to be a bit cheeky with him. I forward deployed him out of cover, as far forward as I could on the left side of the table. This put him in a spot where he could see 3 of Russ' core link members, and shooting any of them would certainly splash others with the template. It was a play that seemed likely to fail, but it'd waste a lot of Russ' time, and if it *did* succeed then I figured it'd basically win me the game.

## Round One

At the top of turn one, I spent a command token to limit Russ' use of command tokens during his turn. I knew he'd *have* to break his core link to deal with the Agema, so limiting his command token usage meant that he wouldn't be able to reform that link again if later in the turn he caught some bad dice.

As I'd hoped, Russ wasn't able to achieve very much on his turn. He spent half his orders throwing smoke to avoid my flash pulse bot, then moving up with the Daturazi, Zerat, and Xenotech, and activating the console in the back right quadrant. With the second half of his orders, he broke his core link, wiggled someone prone out of template range, then took out the Agema with his HMG. He then reformed the link and moved them up to just outside the big centre-left building.

On my turn, I noticed that Russ hadn't quite left himself enough orders to properly hide all his core link. A few of his members were in a nice neat line along the side of a building, and there was nobody covering the right side approach that would get me within 16 inches of his HRL. I moved up Hector's link and killed the HRL with plasma. I took a pot shot at the Daturazi on my way, but he successfully dodged with a smoke grenade.

I wanted to continue up the table killing the rest of the link, but unfortunately, my dice disagreed with me. I was trying to fight the Suryat HMG in my good range, shooting from cover, with him out of cover. He crit Hector in the first exchange, then kept making his ARM and BTS saves. At least the Raktorak I splashed with the plasma template died!

I knew that once the Suryat HMG was down, my opponent would have nothing capable of taking out Phoenix at range in his active, so I kept throwing orders into it. He finally took his second wound when I had about three orders left and had already started pulling my link back to my side of the table. Unfortunately, this meant I didn't have the orders to double tap him and stop his doctor from picking him back up.

The Suryat soaking up so many shots also meant that I didn't have the orders to properly engage Russ' two remaining flamer-equipped link members. I didn't want him to be able to burn my Myrmidons, so I moved my link all the way back into my deployment zone where they started. I was just thinking about surviving when I did this, not about points, so I foolishly forgot to leave anyone in the zone containing my Xenotech. Whoops!

The first round ended 4-1 in Russ' favour.

## Round Two

For Russ' second turn, he first spent some orders moving up a slave drone, healing the downed HMG, and activating the console in that back left quadrant. He then reformed the link and moved it into the big centre-left building. He left everyone hiding behind corners and doorways, hoping to get in some flamer and shotgun hits.

He didn't have the orders to move up and fight me with what was left of his core link, so he tried to bring on his AD troop at this point. I think he underestimated Thrasymedes here; the poor Rasyat was immediately wasted by a rocket.

For my second turn, I decided it was time to take out the flamers that had kept my Myrmidon link bunkered. I spec-fired a smoke grenade into the building, then moved in and killed everything with swords. I was very worried about putting Russ into retreat at this point, so when a Suryat managed to crit Hector in CC, I just broke and reformed the link without Hector and left the two of them engaged. This turned out to be the right call; if I'd killed the Suryat, I wouldn't have gotten a third turn and would've lost this game.

I was a bit worried about not getting enough OPs at this point, so I had Thrasymedes come out of his hiding place, move up and secure a console.

This round ended with me controlling more zones, and we both had zones with our Xenotech in them. Russ had two consoles to my one. So 5-3 his way.

## Round Three

Russ was very short on orders for his final turn. He moved his Zerat up to take a shot at Thrasymedes, but neither was able to score a hit on the other.

For my third turn, I activated consoles with my Myrmidon hacker. Hector dispatched the Suryat, then wandered over to Russ' HVT to get my classified (Follow Up). IIRC the Daturazi, Zerat, and Thorakitai exchanged some shots as well, but it was mostly insignificant.

The game ended 9-6 my way. A minor victory for me.

## What went well

 - I managed my opponent's core link really well this game. He had a lot of scary weapons in that link; flamethrowers and an HMG in particular. He never got the opportunity to bring either to bear against me.
 - Relatedly, throwing smoke and charging down Russ' flamethrowers was a great move. I don't think there was any other safe way of dealing with them.
 - I deployed really well! Especially against a limited insertion list, there was just no way Russ was able to threaten any of my important pieces in his first turn. There were too many good AROs for him to fight through.
 - Thrasymedes did a great job of holding down the entire right flank against a Rasyat, Daturazi, and Zerat. I never had to worry about my Myrmidons being outmaneuvered.

## What went badly

Dominating no zones in the first round was really bad. I'm glad I pulled the Myrmidons back, but I could've at least left the 16 point chain rifle guy out to get some OPs. Or perhaps I could've pulled them back into the back right side of the table instead of my deployment zone?

## Where I got lucky

Hard to say. I do feel like I got pretty bad dice this game. For example, the shootout with the Suryat HMG [should not have taken ~4 orders](http://inf-dice.ghostlords.com/n3/?p1.faction=Aleph&p1.unit=Hector%2C+Homerid+Champion&p1.w_type=W&p1.type=HI&p1.cc=23&p1.bs=13&p1.ph=14&p1.wip=15&p1.arm=5&p1.bts=6&p1.w=2&p1.w_taken=0&p1.nwi=1&p1.symbiont=0&p1.operator=0&p1.immunity=&p1.hyperdynamics=0&p1.ikohl=0&p1.ch=0&p1.msv=0&p1.marksmanship=0&p1.xvisor=1&p1.fatality=0&p1.full_auto=0&p1.surprise=0&p1.action=bs&p1.weapon=Plasma+Rifle&p1.stat=BS&p1.ammo=Plasma&p1.b=3&p1.dam=13&p1.template=1&p1.range=0-8%2F%2B3&p1.link=3&p1.viz=0&p1.ma=0&p1.guard=0&p1.protheion=0&p1.nbw=0&p1.gang_up=0&p1.coordinated=0&p1.cover=3&p1.misc_mod=0&p2.faction=Combined+Army&p2.unit=Suryats&p2.w_type=W&p2.type=HI&p2.cc=16&p2.bs=13&p2.ph=13&p2.wip=13&p2.arm=4&p2.bts=6&p2.w=2&p2.w_taken=0&p2.symbiont=0&p2.operator=0&p2.immunity=&p2.hyperdynamics=0&p2.ikohl=0&p2.ch=0&p2.msv=0&p2.marksmanship=0&p2.xvisor=0&p2.fatality=0&p2.full_auto=0&p2.surprise=0&p2.action=bs&p2.weapon=Pistol&p2.stat=BS&p2.ammo=Normal&p2.b=1&p2.dam=11&p2.range=0-8%2F%2B3&p2.link=3&p2.viz=0&p2.ma=0&p2.guard=0&p2.protheion=0&p2.nbw=0&p2.gang_up=0&p2.coordinated=0&p2.misc_mod=0).
