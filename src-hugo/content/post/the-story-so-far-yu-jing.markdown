+++
Tags = ["infinity"]
date = "Mon, 18 Jun 2018 19:03:14 +1000"
draft = false
title = "The Story so far: Yu Jing"
+++

{{< img src="infinity/yujing.jpg" title="All my Yu Jing at time of writing" >}}

I got into Infinity through the Operation: Red Veil box set. I picked it up on a
a whim when I saw it at the store, after having read a fair bit about the game
online.

Not only was Red Veil my first introduction with Infinity, it was my
(re?)introduction to miniature wargaming as a whole. Like a lot of other people
in the hobby, I grew up with older brothers who played a bit of Warhammer. I had
a terribly painted set of Tau, but never got to really play myself. By the time
I was old enough to have an idea of the rules, they'd moved on.

<!--more-->

{{< img src="infinity/zhanshi-new.jpg" title="Zhanshi! My first painted Infinity miniatures" >}}

I suppose Infinity appealed to me as a way of having all that fun that I got to
observe as a kid, but never be a proper part of.

I took the Yu Jing half of the set, and Daniel took the Haqqislam. In the end I
do all the buying, assembling, and painting in our relationship, but that
division still holds in my head.

{{< img src="infinity/hsien.jpg" title="My pre-repaint Hsien Warrior" >}}

My first attempts at painting Infinity miniatures went... okay. See for yourself
I suppose.

I hadn't quite worked out highlights; I went so subtle at the time! I also had
no idea how to paint orange, in particular I didn't know how I was supposed to
highlight something that was already bright.

But I persisted, finishing off painting the set and teaching both myself and
Daniel the game.

{{< img src="infinity/rawr-new.jpg" title="My old Tiger Soldier" >}}

{{< img src="infinity/zuyong-new.jpg" title="My old Zuyong. He's actually not terrible!" >}}

From there I dipped into JSA, which I'm sure I'll get to in another post. A
simpler scheme, without any orange to paint!

Occasionally I'd dip back into the vanilla Yu Jing colour scheme when I had some
unit to paint that I thought was useful to both. Remotes, doctors, etc. At some
point in all this I finally cracked how to paint orange!

{{< img src="infinity/jsa-support.jpg" title="My JSA doctor, engineer, and yaozao" >}}

(For reference, it's Vallejo Orange Brown, washed with Agrax Earthshade, then
do super blunt highlights in white, then glaze Vallejo Scrofulous Brown all over
the place)

A long while later, my local Infinity club started running the 2018 annual slow
grow league. Yu Jing aren't exactly new for me, but I figured it was a good
excuse to give my original Red Veil set another go.

I'd not stripped models before, and was a bit nervous about doing so. How can I
see my own progress if I don't keep older attempts? In retrospect I do think it
was a great decision. I'm way, way happier with the models now than I was
before.

{{< img src="infinity/zhanshi-repaint.jpg" title="Zhanshi post-repaint" >}}

Plus I've still got all the old pictures.

I'm also taking the opportunity to expand my army a bit, since the Uprising
narrative event pretty much gutted the lists that I used to run when learning
the game. I miss my poor Raiden!

{{< img src="infinity/redveil-repaint.jpg" title="All the parts of Red Veil I've repainted so far" >}}

{{< img src="infinity/shaolin.jpg" title="Shaolin Warrior Monks" >}}

{{< img src="infinity/kanren.jpg" title="Kanren" >}}

Right now I'm working on finishing up a solid base for Yu Jing lists. From there
I can focus on some of my other projects and just paint the odd Yu Jing model
for the slow grow's monthly painting contest.

Right now my painting queue includes:

 - Two more tiger soldiers
 - Another Zuyong, this one with an HMG
 - A Dao Fei
 - A Yan Huo with missile launchers, although he's earmarked for one of the
 painting contents in a later month
 - A ninja

I also need to get around to rebasing my Yaokong remotes, which are still on the
old pre-Uprising grass bases. We can't be sharing basing schemes with the
traitorous JSA!

