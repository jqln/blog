+++
Tags = ["infinity", "Cancon 2019"]
date = "2019-01-31"
draft = false
title = "Cancon 2019: Lists"
+++

List building for Cancon was a little bit of a mess for me. I had two really tight, competitive lists that I think would've done well. But then about a week before, I decided I didn't like them. They were good lists, but they weren't lists *for me*. They didn't play the way that I like to play Steel Phalanx.

So a week before the biggest tournament of the year, I completely threw out my lists and built two new ones.

<!--more-->

Some background on my list-building methodology: I believe that Steel Phalanx is an extremely versatile faction on a per-list basis. Most SP lists I build are capable of comfortably taking on any mission in a tournament lineup. The difficulty with SP comes from adjusting to the various kinds of threats that different factions can project against you.

For example, a high order count Ariadna list typically requires SP to bring access to sensor and some disposable ARO pieces in order to fight effectively. But against higher tech factions like Combined Army, I've found SP does better with lower order count, higher quality troops.

# List 1: Cat

{{< img-noscale src="cancon-2019/list-1.png" title="Screenshot of my first army list" >}}

My first list is a high order count Hector list. It's designed to be able to fight most things, whilst also having enough left over orders to complete the Xenotech objective in missions like Supremacy and Frontline. With Hector and a big second combat group, it also has the durability it needs to keep throwing huge numbers of orders into the Myrmidon link late into the game.

There's a couple of Cancon-specific considerations that went into this list:

 - I've gone for a pair of ordinary SMG-equipped Thorakitai over the FO profiles, even though I've got the points for FOs. The rationale for this is that my Thorakitai can be a bit of a softer underbelly in a list like this, and I don't want to give up easy points in Firefight.
 - I've included a Warcor in the second group to sync with the Xenotech. Normally I'd use those points the give the Agema a multi sniper instead of a missile launcher, but I couldn't quite fit both into this list. I really feel like I needed the Warcor since, without it, the Agema is the only valid trooper who can CivEvac (everyone else is either a REM, or in a link team).

# List 2: Dog

{{< img-noscale src="cancon-2019/list-2.png" title="Screenshot of my second army list" >}}

I honestly didn't put a lot of thought into this list. I wanted to have something comfortable and easy that I could fall back on if I lost the first few rounds; something that'd be fun to play and fun for my opponent to go up against.

It wants to stick Achilles somewhere in the midfield in suppression, supported by the Agema and two link teams. It doesn't have any of kind of ablative ARO pieces like the first list does, so if it loses the first turn then it needs to bunker hard and hope for the best.

As it shook out, I did well enough in the tournament that I didn't end up needing to bring out this list. My opponents also seemed good enough each round that I was a little uncomfortable using an Achilles list; I felt most of my opponents would be very comfortable with handling the kind of threat that he projects.

