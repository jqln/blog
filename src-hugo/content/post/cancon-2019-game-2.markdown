+++
Tags = ["infinity", "Cancon 2019"]
date = "2019-02-02"
draft = false
title = "Cancon 2019: Game Two (Decapitation)"
+++

My second game was against Jon, also from Melbourne. He was running JSA. The table we played on was pretty brutal for Decapitation:

{{< img src="cancon-2019/round-2-table.jpg" title="Table for game 2. Photo by Mark Fabian, I'm not sure whose table this is" >}}

<!--more-->

Jon's list was fairly straightforward IMO. He had a defensive core link of Keisotsu, an aggressive Daiyoukai/Domaru Haris team (including Neko, his Lt), plus a few support pieces. I don't know how his combat groups were arranged, but his list was something like this:

{{< img-noscale src="cancon-2019/round-2-list.png" title="An estimate of my opponent's list" >}}

(I'm about 5 points short here. I must be missing a G:Servant bot, or maybe one of the other Keisotsu was a specialist. The SWC allocation seems right.)

I used my Hector list because that is the only list I used all Cancon. In particular, the Hector list has a good, strong datatracker (Hector!) and plenty of ARO pieces to help defend the HVT.

I lost the initiative roll this game. Jon, of course, chose to go first. I took the table edge at the top left of the photo, since as far as I could tell it was the only side that would let me get a decent LOF on units gunning for my HVT.

I deployed my HVT on my far left (top right in the pic), behind the shipping container. I deployed with my HVT very heavily bunkered; a linked Thorakites SMG was standing next to him, Thrasymedes was standing in a doorway covering both, Phoenix and the Agema were out in the street covering the long firelane that units would have to pass through to come around the container.

I also had a chain rifle Myrmidon on the street near the shipping container. I rationalised that if Jon wanted to actually try to fight his way through to my HVT (as opposed to sacrificing his data tracker), the Myrmidon could drop smoke for the Agema to shoot through in ARO.

Jon deployed his Daiyoukai link on the building with the bridge in his deployment zone. The Keisotsu link went down on the other building he had access to, with the missile launcher watching over most of the table. His Ryuken-9 SMG was forward deployed on the objective room near the centre, and he had a smattering of camo tokens all around the front of his deployment zone.

Jon placed his HVT in essentially the same place as mine, just on the other side of the shipping container. It was a crumby position, but I don't think he actually had any better spots.

The Daiyoukai was Jon's data tracker.

## Round 1

I started by stripping two orders from the combat group with Jon's data tracker. As expected, he started the game by pushing hard on my left side with the Daiyoukai link. He killed a flash pulse bot with his Red Fury, then spent an order trading shots with Phoenix to no effect.

By this point, he only had a few orders left in his Datatracker's combat group. As I'd hoped, this just wasn't enough for him to fight past everything I had. He revealed an Oniwaban next to my HVT, who killed my bodyguard Thorakitai and shrugged off a shot from Thrasymedes. The Dayoukai then made a suicide run at my HVT. The HVT went down, but even a Dayoukai has trouble shrugging off multiple rockets and missiles on normal rolls.

Jon then spent the rest of his turn running back the surviving Domaru, trying to get his Lt back to safety. Neko ended up out of cover, in the building at the back-left (my POV) of the table. He ended up with a couple of spare orders to spend on his midfield Ryuken, which he spent to climb down off a building and take a shot at a Netrod (missing due to bad range).

At the top of my turn, I was feeling quite good. I had 10 orders on my Hector/Phoenix link, and I could get to both Jon's HVT and Lt relatively safely. All he had stopping me was his Keisotsu missile launcher, who I could get by with one smoke grenade, and a Domaru with Chain Rifle, who Hector could easily take out.

It did not go well.

The first part of my turn was good. Phoenix got his smoke down near the shipping container, allowing me to slip past the Keisotsu. Hector then melted Jon's HVT with B4 plasma in good range.

Hector then completely bounced off the lone Domaru between me and Neko. I spent... maybe 4 or 5 orders shooting the Domaru? Jon kept rolling 13s/14s on his B1 E/M grenades. The end result was the Domaru had taken one wound, and Hector was immobilized.

I spend the rest of my first combat group orders setting up defensively around Hector. Phoenix was watching the long approach down the left side of the table, my chain rifle was looking around the other side of the building, my assault hacker was behind a box, overlooking Hector.

At some point in all this, I also spent the order to have Thrasymedes take out Jon's Oniwaban.

I hadn't spent any orders on my second group at this point. I moved the Agema out to take a pot shot at Jon's exposed Ryuken. The first shot failed, but the second shot connected and obliterated the poor Ryuken.

## Round 2

For Jon's second turn, he didn't make any particularly big plays. He basically just ran Neko all the way to the other side of the table to safety, then his other Ryuken dropped a few mines. Hector had been isolated during my turn, meaning I didn't actually have an Lt for Jon to go for during his turn. I believe it was during this turn that the Keisotsu and Agema missile launchers fought each other, with my Agema losing.

On my second turn, I nominated a Thorakites prone on a building as my Lt. I didn't have the orders to clear mines and AROs pieces, then push, then end up in a good spot defending Hector, so I also played this turn quite conservatively.

Phoenix peeked out at the Keisotsu missile launcher and exchanged some shots. This, as I was coming to expect, took way longer than it had any right to. The Keisotsu burned two or three of my orders just by tanking all his armour saves from the HRL. She did eventually go down though.

Phoenix then took a shot at Jon's remaining Ryuken, who had been left standing next to one of the mines he's dropped. Both the Ryuken and the mine went down.

After this, I bunkered everyone back down to where they'd started the turn. Notably, the Domaru was still alive just around the corner from my Myrmidon link.

## Round 3

Jon started his third turn by having Yuriko peek out to shoot at Phoenix. After he'd poked his head out, he realised the range was probably bad for his combi, so he elected to drop a mine instead of shooting. The range *was* bad; if he'd shot, he would've been unable to succeed (-6 for ODD, -3 for cover, -3 for bad combi rifle range).

Not having much else to do, Jon decided to make a run at my new Lt with his Domaru. This came scarily close to working. First, he poked his head out at two of my Myrmidons, and the three all traded direct templates with one another. My chain rifle Myrmidon went down, and my Assault Hacker made her save. Jon's Domaru made all four(!) nanopulser saves. He then continued on, running for the building with my Lt, but thankfully the next round of double nanopulsers brought him down.

At the start of my turn, I counted up points and figured that I'd won the game at this point. Hector had been isolated, but not killed, so neither of us had killed any Lts. We'd both killed the HVT with our data trackers, so the game was going to come down to army points killed, which I was confident I was ahead on.

Just to be sure, I spent my turn moving up Phoenix to stab Jon's doctor in CC. As I expected, I won a 7-5 minor victory. On army points, I had 207 left, whilst Jon had 127.


## What went well

 - Jon had to sacrifice both his Daiyokai and an Oniwaban to kill the designated target. That put me about 90 points up in the game, right from the start.
 - After Hector was immobilized, I did a good job of defending him. He'd have otherwise been an easy target, and if he'd gone down I would've lost the game on army points.

## What went badly

 - Hector should've won his fight with the Domaru, but I could've stacked the odds even more in his favour. I probably should've done so once I realized the Domaru actually had a decent ARO with his EM grenades.
 - Thinking even more about this interaction, I really should've just immobilized the Domaru with my AHD, then shot him on normal rolls. I definitely need to remember to use that hacker more.
 - This was the first of many games where I was unsatisfied with the Agema ML. When he works, he's brutally effective. But B1 in the active turn is just too inconsistent to rely on. If he'd taken out the Ryuken in one shot, I would've had the order to move him back into safety for Jon's turn.

