+++
Tags = ["infinity", "Cancon 2019"]
date = "2019-02-03"
draft = false
title = "Cancon 2019: Game Three (Frontline)"
+++

My third game was against Jonathan, again from Melbourne! He was running vanilla Haqqislam. Specifically, vanilla Haqqislam with Saladin and two Fidays. We played on this table:

{{< img src="cancon-2019/round-3-table.jpg" title="Table for game 3. Photo by Mark Fabian, table by Luke and Peter Henry" >}}

Jonathan also won the initiative roll, and chose to go first. I promise this game went better than it seems like it's about to.

<!--more-->

I'm not sure enough about Jonathan's list to try mocking it up in the army builder, but it definitely contained:

 - A Djanbazan sniper
 - 4 Ghazi
 - 2 Fidays
 - Saladin
 - A Taureg
 - A Hunzakut
 - A Libertos

He also had several flash pulse and baggage remotes, something holoprojectored as a Djanbazan HMG (it never revealed), and *a lot* of camo markers.

Jonathan deployed first, and I made him deploy on the closest side of the photo above. His fake Djanbazan HMG was on the far left, behind the arch/gate thing. Saladin and the Djanbazan sniper were placed on the roof of the building on his right, with a collection of remotes hiding behind the wall behind the building. A huge number of camo tokens went down all over the centre line of the table. Two on his left side, four or so in the middle, one or two on his right.

All four ghazi deployed on Jonathan's right, mostly placed such that they could move impetuously and throw smoke without necessarily exposing themselves to AROs.

I deployed my Myrmidon link behind the big curved building in the middle of my deployment zone. The Thorakitai link forward deployed on and around the building on the far right of my deployment zone with my Warcor, with the Agema missile launcher peeking out behind the spire on the roof. My remotes were all deployed peeking out from behind corners.

The intention of my deployment was that anything (say, 4 Ghazi...) making a run for my Myrmidon link would have to first fight my Agema, flash pulse bots, and Thrasymedes.

Jonathan's reserves were, of course, his two Fidays. He deployed one on the central building, closer to my side of the table. He deployed the other on the building with my Thorakitai link.

## Round 1

I started by stripping two orders from the combat group with the Ghazi. This left that group with IIRC just one regular order; not enough to make a run for Hector.

The Ghazi ran forward slightly, but couldn't be seen by anything on my side (except perhaps a flash pulse bot?). Jonathan then went to work with his Fiday.

First, he exploited some rather poor deployment on my part: he was able to position his Fiday such that my Warcor was placed perfectly between the Fiday and a Thorakites, preventing me from shooting back with my chain rifle. Jonathan then took out both the Thorakites and Warcor with a light shotgun. After this, he reentered Impersonation, crept about and dropped a mine next to one of my remaining Thorakitai. He then reentered impersonation again, climbed up a ladder and killed my Agema. The Fiday ended the turn prone on the roof where my Agema had been deployed.

For the rest of his turn, all Jonathan did was move forward a few camo markers, drop some mines, and reveal a Hunzakut but placing it on suppression on my left side.

I started my turn by moving a remote into my big combat group, rounding that group out to 11 orders (including Hector's Strategos order). I then saw a line that Jonathan hadn't covered well: if I made it past one Ghazi and his Hunzakut, I'd have a clear run up my far left side that would take me all the way up to Saladin. Along the way, I'd even get a chance to make shots at his Ghazi.

This plan did not go well. The Hunzakut went down easily enough to Phoenix, but the lone Ghazi on that side simply would not die. First Phoenix shot him for two orders. The Ghazi tanked the first order of hits, then in the next order, he successfully dodged closer. I tried to use my second combat group orders to flash pulse the Ghazi, but he made his BTS saves. Finally, I had to break my link (to prevent activating Phoenix), bring Hector around to fight from outside Jammer range, and kill the Ghazi with his plasma rifle. And even then, IIRC it still took two orders for Hector to kill it.

At this point, I didn't have enough orders to finish my plan. If I'd pressed on, I'd have been left badly out of position against the remaining Ghazi. I pulled my Myrmidons back to where they started.

## Round 2

Jonathan's second turn was mostly focussed on setting up more barriers to prevent me from advancing. A regular camo marker on my far left revealed itself to be a Taureg. It moved up towards my deployment zone and dropped a couple of mines. Another camo marker moved up on my right side, killing my LRL Thorakites.

Otherwise, Jonathan spent this turn crawling his Djanbazan and Saladin out of his deployment zone, and placing his multi-scanner.

For my second turn, I tried that same left side push that hadn't worked out last time. This time instead of a suppressing Hunzakut and Ghazi, there was a Taureg, its mines, and a new Ghazi.

First I dropped a sniffer and used Sensor to reveal the mine that the Taureg was prone next to. Hector then moved up and shot the mine with plasma, splashing the Taureg with the blast template.

I rolled two crits with Hector's shots, but the Taureg crit his dodge. At this point in the day, I was pretty convinced that my dice were under some kind of curse.

For my next order, I moved Hector back out of LoF from the Taureg so that next order I'd be able to use my Assault Hacker to shoot instead (taking advantage of ODD to stack mods). Thankfully this wasn't needed, and the Taureg went down to Hector's second volley.

At this point, I was down more orders than I would've liked, but I still thought I had just enough to go after Saladin. I might've ended up out of position, but putting my opponent into LoL on his final turn in Frontline seemed worth it. Hector's link pushed up on my far left, and once again I encountered an Invincible Ghazi.

Hector fired a volley of plasma, the Ghazi successfully dodged closer. Hector fired again, the Ghazi died. But before he died, the Ghazi dropped an E/Marat on Hector to Isolate and Immobilize him.

 I spent the rest of this turn putting my Myrmidons into a defensive position around Hector (an IMM-2'd Hector is still 70 odd points in the middle zone!). I had a spare order left over, so I had Thrasymedes walk over and kill the Farzan(?) that took out my LRL on Jonathan's last turn.

## Round 3

Jonathan spent his final turn pushing models into zones for scoring purposes and dropping a couple of mines near the Myrmidon link. He had Saladin move up to take a cheeky shot at Phoenix, and of course, Phoenix got crit. I chose to fail the guts roll and move back out of sight.

At the top of my third turn, we only had about two minutes left in the game. I was quite panicked by this and tried to finish my turn as quickly as possible. In retrospect this was a big mistake, I should've just taken my time and properly assessed the situation.

First, I had my chain rifle myrmidon move into the middle zone, attempting to dodge the mine that was locking that area down. He failed his dodge, then failed his armour save and died. Phoenix then also tried to move up into the second zone. He failed to throw smoke, then got crit by Saladin and died.

I spent most of my irregular orders bringing Thrasymedes and my surviving remotes into the zone closest to me. I thought I had this zone - I knew I had more than the ~35 points cost of one Fiday, but I forgot Jonathan still had a second in this zone! Thinking I needed more points in the middle zone (I didn't), I then pushed my Assault Hacker into the centre.

I lost this game 1-6.

## What went well

 - Jonathan's Ghazi were unable to reach my Myrmidon link turn 1, which meant I always had the ability to make big pushes in my active turn.
 - Aside from the misplaced Warcor, I think my deployment was really solid this game. Jonathan came at me with two reserve deployment Fidays, and all I lost was a Thorakites, and an Agema and Warcor from my second group.
 - Pulling my Myrmidons back at the end of my first turn was the right play. If I'd bunkered them in the midfield, they'd have been surrounded by mines and then Isolated by Ghazi. If I'd left them in the open in the midfield, the Djanbazan sniper would've had no trouble picking them all off one by one.

## What went badly

 - Rushing through my last turn to make the game's time limit lost me this game. I should've taken my time and really thought about what I needed to do.
 - I deployed my Warcor more as an afterthought, after working out where my main links were going to go down. As a result, it was placed in a really silly way. If he was just an inch more to the right, I could've totally shut down the Fiday (and probably would've managed to complete the Xenotech objective).
 - I probably should've tried flash pulsing Jonathan's Ghazi *before* moving the Myrmidon link up. If successful, I'd have saved a couple of orders by not needing to make that fight at such a range.

