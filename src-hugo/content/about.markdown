+++
date = 2014-06-09T05:03:41Z
draft = false
title = ""
+++

## what is this?

This is my website! Welcome! I blog (very!) infrequently about whatever is on my
mind. Sometimes that means battle reports from miniatures games, sometimes that
means miniature painting content, sometimes it'll be a book review, or some kind
of unhinged ramble.

I miss when the internet was a larger place, where people had their own corners
where they did what they wanted. Maybe that idea stays alive, so long as people
keep writing very strange blogs on websites that nobody visits.

## who am i?

My name is Jacqueline (pronouns she/they). Hello! I live on Wallumedegal land in
the [Eora nation], with my partner Daniel. I usually support myself by working
as a Software Engineer, although I'm currently unemployed. I entertain myself
with all sorts of tabletop hobbies on the side.

In terms of wargaming, these days I mostly play Infinity, although I'm trying to
diversify a little by getting into Age of Sigmar.

If you'd like to chat, the best way to contact me is to DM me on Twitter,
[@cooljqln]. For can also email me at [me@jacqueline.id.au].

## you're a boring nerd

If you're interested in how the blog is put together on the technical side, the
entire source for it is [available on Bitbucket]. It uses a combination of
[Hugo] for most of the site generation with [Gulp] to help out with the bits
Hugo can't do. Generating images in different sizes, compiling Sass, that sort
of thing.

[Eora nation]: https://en.wikipedia.org/wiki/Eora
[me@jacqueline.id.au]: mailto:me@jacqueline.id.au
[@cooljqln]: https://twitter.com/cooljqln
[available on Bitbucket]: https://bitbucket.org/jqln/blog
[Hugo]: http://hugo.spf13.com/
[Gulp]: http://gulpjs.com/
