#!/bin/bash
set -e

if [ "." != "`dirname $0`" ]; then
  echo "Fatal error: script must be run from the directory that contains it"
  exit 1
fi

echo "Cleaning out old build artifacts"
rm -rf out

echo "Building and assembling site"
gulp build
hugo

echo "Done building!"
