#!/bin/bash
set -e

if [ "." != "`dirname $0`" ]; then
  echo "Fatal error: script must be run from the directory that contains it"
  exit 1
fi

echo "Building site"
./build.sh

echo "Deploying to netlify"
NETLIFY_SITE_ID=e1be20d1-b065-499e-9774-d5ee3945795c netlify deploy --dir=out/assembled --prod

echo "Done deploying!"
