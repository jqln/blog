#!/bin/bash
gulp &
sleep 1
hugo serve -b "http://127.0.0.1/" -D

# For previewing locally.
# hugo serve --bind 192.168.2.93 -b "http://192.168.2.93/" -D
