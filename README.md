# blog #

This is the repository in which I keep my blog. You can see it hosted at
[jacqueline.id.au](https://jacqueline.id.au/).


### Dependencies ###

If for whatever reason you want to build this, you'll need the following things:

- [hugo](http://hugo.spf13.com/) (v0.17 works, newer versions should too)
- [gulp](http://gulpjs.com)
- ImageMagick

### Building ###

First you'll need to install of all the bits and bobs used by gulp:

```
npm install
```

Then just run `watch.sh` and it should build everything and start up a server
with livereload.

### Directory Structure ###

`archetypes` - See archetype's in Hugo's docs. Basically templates for different
kinds of content.

`content` - Site content. Blog posts, about page, etc. In markdown format.

`css` - Stylesheets for the site. Despite the name, may contain sass files.

`css-partials` - Sources for any scss partials referenced in `css`.

`img` - static image resources

`img-resize` - larger images that need to be resized into thumbnail and 'full'
sizes before we can use them.

`js` - Javascript sources.

`layouts` - HTML templates used by Hugo to render the site.

`root` - Extra files to be copied to the root directory of the generated site.

There are some additional files in the root directory:

`config.yaml` - Hugo site configuration.

`deploy.sh` - Script to build the site and push it out.

`gulpfile.js` - Defines the build process for the site.

`package.json` - Specifies which npm packages we depend on.

`README.md` - This file. Hello!

`watch.sh` - Builds the site and hosts a local server, regenerating the site and
refreshing the browser (via livereload) as needed.

After building, the following extra directories will exist:

`out/intermediates` - Hugo's static files directory. Contains all the copied and

generated stylesheets/images/javascript from the root folder.
`out/assembled` - The final, complete site. Shove this on a server.

